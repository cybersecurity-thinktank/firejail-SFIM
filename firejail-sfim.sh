#!/bin/sh
set -x #echo on
#HOWTO:
#1° make sure the $PATH contains the $HOME/bin directory first
#This is done in Linux Mint with the following code put in ~/.profile
# set PATH so it includes user's private bin if it exists
#if [ -d "$HOME/bin" ] ; then
#    PATH="$HOME/bin:$PATH"
#fi
#2° copy paste this file in the $HOME/bin directory calling it firejail-sfim.sh
#3° limit the right to the script to read and execute only for the user: chmod 500 firejail-sfim.sh
#4° make symbolic links to all the programs you want to execute this way: cd $HOME/bin; ln -s firejail-sfim.sh vlc
#5° have fun :)

#put this in /usr/local/bin Name it "vlc" so that it launches vlc and so on
#do not forget to "chmod +x <script>" the script

#@DEBUG: This script currently only works when there is a single parameter, that is the file to open. E.g.: vlc openThis.mkv
	# To make it work with VLC while double clicking, please edit
		# /usr/share/applications/vlc.desktop
		# ~/.local/share/applications/vlc.desktop
	# and change
		# Exec=/usr/bin/vlc --started-from-file %U
	# to
		# Exec=/usr/bin/vlc %U

########### DEBUG
# DEBUGVAR=$@
# echo "DEBUG :$DEBUGVAR"
# DEBUGVAR="$DEBUGVAR ----- $0"
# echo "DEBUG :$0"
# DEBUGVAR="$DEBUGVAR ----- $1"
# echo "DEBUG :$1"
# echo "DEBUG :$2"
# echo "DEBUG :$3"
########### DEBUG

#Recovering parameters
programToStart=$(basename "$0")
echo $programToStart
if [ -z "$@" ] #if no file parameter
then
	echo ""
else
	srcFileToOpen=$(readlink -m "$@")

	########### DEBUG
	echo "DEBUG : Path to file to open $srcFileToOpen"
	# DEBUGVAR="$DEBUGVAR ----- $srcFileToOpen"
	# DEBUGVAR="$DEBUGVAR ----- $fileDirectory"
	########### DEBUG

	filename="$(basename "$srcFileToOpen")"
	fileDirectory="$(dirname "$srcFileToOpen")"

	# The variables below are used to check for potential subtitles in case this script is used with VLC
	filenameNoExtension="${filename%.*}"
	filenameSRT="$filenameNoExtension.srt"
	srcFileSubtitlesToOpen="$fileDirectory/$filenameSRT"


	########### DEBUG
	# echo "DEBUG: File to open $filename"
	# DEBUGVAR="$DEBUGVAR ----- $filename"
        # DEBUGVAR="$DEBUGVAR ----- $fileDirectory"
        # echo "DEBUG: Directory of that file $fileDirectory"
	# echo "DEBUG: Potential subtitle $filenameSRT"
	# echo "DEBUG: Potential subtitle full path $srcFileSubtitlesToOpen"
	# DEBUGVAR="$DEBUGVAR ---- $filenameSRT"
	########### DEBUG

fi

# Creates a temporary directory within the same directory as the file to maximise the chances to be able to hardlink to that file.
cd $(mktemp -d -p "$fileDirectory")
directoryname="$(pwd)"

########### DEBUG
# echo "DEBUG: Temporary directory: $directoryname"
# DEBUGVAR="$DEBUGVAR ----- $directoryname"
########### DEBUG

# Makes a hard link to the file to open in that temporary directory
if [ -z "$@" ] #if no file parameter
then
	echo ""
else
	ln "$srcFileToOpen" "$filename"
	#do the same for potential SRT subtitle that has the same name as the file to open
	ln "$srcFileSubtitlesToOpen" "$filenameSRT"
fi

########### DEBUG
# echo "DEBUG: Content of TMP directory"
# ls -la $directoryname >> debug.txt
# echo $DEBUGVAR >> debug.txt
# echo $directoryname >> debug.txt
########### DEBUG

# Launches the program on that hardlink
if [ -z "$@" ] #if no file parameter
then
	firejail --hostname=$programToStart --private="$directoryname" "$programToStart"
else
	firejail --hostname=$programToStart --private="$directoryname" "$programToStart" "$filename"
fi

# Erases the temporary directory and everything in it
rm -rf $(pwd)