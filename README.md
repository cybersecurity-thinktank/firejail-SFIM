Use at your own risk. Experimental script.
# Installation
Copy the script in ~/bin/.
Rename it or create a logical link with the name of the program you want to be controlled by the script (such as evince or vlc).

# Usage
Once installed, when you double clic on a file to be opened, the script will be called, it will:
* create a temporary directory in the current directory
* make a harklink to the file you wanted to open within that directory
* called and sandbox the program and open the file with it
    * the sandboxing is performed using firejail